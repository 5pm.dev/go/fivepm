package fivepm

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

type Team struct {
	CreatedAt *time.Time `json:"created_at,omitempty"`
	Domain    string     `json:"domain,omitempty"`
	Name      string     `json:"name,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	UUID      *uuid.UUID `json:"uuid,omitempty"`

	Credits *Credits                  `json:"credits,omitempty"`
	Keys    map[uuid.UUID]*Key        `json:"keys,omitempty"`    // key is the key's UUID
	Members map[uuid.UUID]*Membership `json:"members,omitempty"` // key is the user's UUID
}

// GetKeys gets all the keys for the team
func (t *Team) GetKeys(ctx context.Context, db database.DB) (*Team, error) {
	ctx, log := logger.New(ctx, "fivepm.Team.GetKeys", logger.WithField("uuid", t.UUID))

	query :=
		`SELECT 
			k."uuid", 
			k."created_at",
			k."updated_at",
			k."name",
			k."description",
			k."active"
		FROM "api_key" k
		INNER JOIN "team" t ON k."team_uuid" = t."uuid"
		WHERE t."uuid" = $1 AND k."deleted" = false;`

	// get the teams for the user
	rows, err := db.Conn.Query(ctx, query, t.UUID)
	if err != nil {
		log.WithError(err).WithField("query", query).Error(ctx, "error getting API Keys")
		return nil, err
	}
	defer rows.Close()

	// loop over the rows and add them to the keys map
	if t.Keys == nil {
		t.Keys = make(map[uuid.UUID]*Key)
	}
	for rows.Next() {
		var key Key

		if err := rows.Scan(
			&key.UUID,
			&key.CreatedAt,
			&key.UpdatedAt,
			&key.Name,
			&key.Description,
			&key.Active,
		); err != nil {
			log.WithError(err).Error(ctx, "error scanning team")
			return nil, err
		}

		t.Keys[*key.UUID] = &key
	}

	return t, nil
}

// Save saves a team creating a new one when necessary
func (t *Team) Save(ctx context.Context, db database.DB) (*Team, error) {
	ctx, log := logger.New(
		ctx,
		"fivepm.Team.Save",
		logger.WithField("domain", t.Domain),
		logger.WithField("name", t.Name),
	)

	// create a new team if the ID is nil, update otherwise
	if t.UUID == nil || *t.UUID == uuid.Nil {
		err := db.Conn.QueryRow(
			ctx,
			`INSERT INTO "team" ("domain", "name") VALUES ($1, $2) RETURNING "uuid";`,
			t.Domain,
			t.Name,
		).Scan(&t.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error creating team")
			return t, err
		}

		// create the default credits for the team
		credits := Credits{
			TeamUUID:  t.UUID,
			Available: BaseCredits,
		}
		if _, err = credits.Save(ctx, db); err != nil {
			log.WithError(err).Error(ctx, "error creating default credits")
			return t, err
		}
	} else {
		err := db.Conn.QueryRow(
			ctx,
			`UPDATE "team" SET "name" = $1 WHERE "uuid" = $2 RETURNING "uuid";`,
			t.Name,
			t.UUID,
		).Scan(&t.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error saving team")
			return t, err
		}
	}

	return t, nil
}
