package fivepm

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
)

// TestValidateToken tests the ValidateToken function
func TestValidateToken(t *testing.T) {
	secret := "testSecret"
	id := uuid.New()
	ctx := context.Background()
	oldUser := User{
		UUID: &id,
	}

	tokenString, _ := oldUser.Tokenize(ctx, secret, time.Now())

	newUser := &User{}
	newUser, err := newUser.ValidateToken(ctx, secret, tokenString)
	if err != nil {
		t.Error("error with the validation")
	}
	if *newUser.UUID != *oldUser.UUID {
		t.Error("profile IDs do not match")
	}
}
