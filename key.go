package fivepm

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

// Key is an API key for a team
type Key struct {
	Active      bool       `json:"active"`
	APIKey      *uuid.UUID `json:"api_key,omitempty"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	Deleted     bool       `json:"deleted"`
	Description string     `json:"description,omitempty"`
	Name        string     `json:"name,omitempty"`
	TeamUUID    *uuid.UUID `json:"team_uuid,omitempty"`
	UpdatedAt   *time.Time `json:"updated_at,omitempty"`
	UserUUID    *uuid.UUID `json:"user_uuid,omitempty"`
	UUID        *uuid.UUID `json:"uuid,omitempty"`
}

// Delete inactivates and markes an API key as deleted
func (k *Key) Delete(ctx context.Context, db database.DB) (*Key, error) {
	ctx, _ = logger.New(ctx, "fivepm.Key.Delete")

	k.Deleted = true
	k.Active = false

	return k.Save(ctx, db)
}

// Save saves a key creating a new one when necessary
func (k *Key) Save(ctx context.Context, db database.DB) (*Key, error) {
	ctx, log := logger.New(ctx, "fivepm.Key.Save")

	// create a new team if the ID is nil, update otherwise
	if k.UUID == nil || *k.UUID == uuid.Nil {
		err := db.Conn.QueryRow(
			ctx,
			`INSERT INTO "api_key" ("team_uuid", "user_uuid", "name", "description") VALUES ($1, $2, $3, $4) RETURNING "uuid", "api_key";`,
			k.TeamUUID,
			k.UserUUID,
			k.Name,
			k.Description,
		).Scan(&k.UUID, &k.APIKey)
		if err != nil {
			log.WithError(err).Error(ctx, "error creating API key")
			return k, err
		}

		k.Active = true // automatically true since it was just created
	} else {
		err := db.Conn.QueryRow(
			ctx,
			`UPDATE "api_key" SET "name" = $1, "description" = $2, "active" = $3, "deleted" = $4, "updated_at" = CURRENT_TIMESTAMP WHERE "uuid" = $5 RETURNING "uuid";`,
			k.Name,
			k.Description,
			k.Active,
			k.Deleted,
			k.UUID,
		).Scan(&k.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error saving API key")
			return k, err
		}
	}

	return k, nil
}
