package fivepm

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

type Role string

const (
	RoleSuper    Role = "SUPER"
	RoleAdmin    Role = "ADMIN"
	RoleUser     Role = "USER"
	RoleReporter Role = "REPORTER"
	RoleGuest    Role = "GUEST"
)

// roles that are able to use enrichment credits
var canEnrich = map[Role]bool{
	RoleSuper: true,
	RoleAdmin: true,
	RoleUser:  true,
}

type Membership struct {
	CreatedAt *time.Time `json:"created_at,omitempty"`
	InvitedBy *uuid.UUID `json:"invited_by,omitempty"`
	Role      Role       `json:"role,omitempty"`
	TeamUUID  *uuid.UUID `json:"team_uuid,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	UserUUID  *uuid.UUID `json:"user_uuid,omitempty"`
	UUID      *uuid.UUID `json:"uuid,omitempty"`
}

// Save saves a teams creating a new one when necessary
func (m *Membership) Save(ctx context.Context, db database.DB) (*Membership, error) {
	ctx, log := logger.New(
		ctx,
		"fivepm.Membership.Save",
		logger.WithField("team_uuid", m.TeamUUID),
		logger.WithField("user_uuid", m.UserUUID),
	)

	// create a new membership if the ID is nil, update otherwise
	if m.UUID == nil || *m.UUID == uuid.Nil {
		err := db.Conn.QueryRow(
			ctx,
			`INSERT INTO "membership" ("invited_by", "role", "team_uuid", "user_uuid") VALUES ($1, $2, $3, $4) RETURNING "uuid";`,
			m.InvitedBy,
			m.Role,
			m.TeamUUID,
			m.UserUUID,
		).Scan(&m.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error creating membership")
			return m, err
		}
	} else {
		err := db.Conn.QueryRow(
			ctx,
			`UPDATE "membership" SET "role" = $1 RETURNING "uuid";`,
			m.Role,
		).Scan(&m.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error saving membership")
			return m, err
		}
	}

	return m, nil
}
