package fivepm

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

const BaseCredits = 100

type Credits struct {
	Available  int        `json:"available,omitempty"`
	CreatedAt  *time.Time `json:"created_at,omitempty"`
	Processing int        `json:"processing,omitempty"`
	TeamUUID   *uuid.UUID `json:"team_uuid,omitempty"`
	UpdatedAt  *time.Time `json:"updated_at,omitempty"`
	UUID       *uuid.UUID `json:"uuid,omitempty"`
}

// Save saves a teams credits creating a new one when necessary
func (c *Credits) Save(ctx context.Context, db database.DB) (*Credits, error) {
	ctx, log := logger.New(
		ctx,
		"fivepm.Credits.Save",
		logger.WithField("credit_uuid", c.UUID),
		logger.WithField("team_uuid", c.TeamUUID),
	)

	// create a new membership if the ID is nil, update otherwise
	if c.UUID == nil || *c.UUID == uuid.Nil {
		err := db.Conn.QueryRow(
			ctx,
			`INSERT INTO "credits" ("team_uuid", "available") VALUES ($1, $2) RETURNING "uuid";`,
			c.TeamUUID,
			c.Available,
		).Scan(&c.UUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error creating credit record")
			return c, err
		}
	} else {
		err := db.Conn.QueryRow(
			ctx,
			`UPDATE "credits" SET "available" = $1, "processing" = $2, "updated_at" = NOW() WHERE "uuid" = $3 RETURNING "uuid";`,
			c.Available,
			c.Processing,
			c.UUID,
		).Scan(&c.UUID)
		if err != nil {
			log.WithError(err).
				WithField("new_credits", c.Available).
				Error(ctx, "error saving credit record")
			return c, err
		}
	}

	return c, nil
}
