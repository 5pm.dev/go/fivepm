package fivepm

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

// ---- API KEY ----

// ValidateAPIKey finds the user details that match an API key if the key is valid
func (u *User) ValidateAPIKey(ctx context.Context, db database.DB, key string, teamUUID uuid.UUID) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.ValidateToken")

	// get the user for the API key and team it it's an active key
	err := db.Conn.QueryRow(
		ctx,
		`SELECT 
			u.uuid,
			u.email,
			u.name
		FROM
			"user" u
			LEFT JOIN "api_key" k ON k."user_uuid" = u."uuid"
		WHERE
			k."api_key" = $1
			AND k."team_uuid" = $2
			AND k."active" = true
		;`,
		key,
		teamUUID,
	).Scan(&u.UUID, &u.Email, &u.Name)
	if err != nil {
		log.WithError(err).Error(ctx, "error validating API key")
		return nil, err
	}

	return u, nil
}

// ---- JWT Auth ----

type claims struct {
	Email string `json:"email"`
	Image string `json:"image"`
	Name  string `json:"name"`
	jwt.RegisteredClaims
}

// Tokenize creates a JWT token for the profile
func (u *User) Tokenize(ctx context.Context, secret string, timestamp time.Time) (string, error) {
	ctx, log := logger.New(ctx, "fivepm.User.Tokenize", logger.WithField("email", u.Email))

	// create the claims
	c := claims{
		Email: u.Email,
		Image: u.Image,
		Name:  u.Name,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer: "5pm.dev",
			// ExpiresAt: jwt.NewNumericDate(timestamp.Add(24 * time.Hour)), // @TODO: set this once token refreshing becomes a thing with the rest of the app
			IssuedAt: jwt.NewNumericDate(timestamp),
		},
	}

	// set claim id if the profile has one and its not empty
	if u.UUID != nil && *u.UUID != uuid.Nil {
		c.RegisteredClaims.ID = u.UUID.String()
	}

	// create the token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.WithError(err).Error(ctx, "error signing token")
		return "", err
	}

	return tokenString, nil
}

// ValidateToken checks the JWT token for the profile to make sure its valid
func (u *User) ValidateToken(ctx context.Context, secret string, tokenString string) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.ValidateToken")

	// remove the bearer from the token
	tokenString = strings.TrimPrefix(tokenString, "Bearer ")

	// parse the token
	var c claims
	token, err := jwt.ParseWithClaims(tokenString, &c, func(token *jwt.Token) (interface{}, error) {
		// don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(secret), nil
	})

	if err != nil {
		log.WithError(err).Error(ctx, "error parsing token")
		return u, err
	}

	// handle invalid token
	if !token.Valid {
		log.WithError(err).Error(ctx, "error looking at claims")
		return u, errors.New("error validating token")
	}

	// @TODO: make sure the token is not expired once token refreshing becomes a thing with the rest of the app

	// parse the id to make sure its valid if its in the token
	if c.RegisteredClaims.ID != "" {
		id, err := uuid.Parse(c.RegisteredClaims.ID)
		if err != nil {
			log.WithError(err).Error(ctx, "error parsing user id")
			return u, err
		}

		u.UUID = &id
	}

	// set the id
	u.Email = c.Email
	u.Image = c.Image
	u.Name = c.Name

	return u, nil
}
