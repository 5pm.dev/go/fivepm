package fivepm

import (
	"context"
	_ "embed"
	"time"

	"github.com/google/uuid"
	"gitlab.com/5pm.dev/go/logger"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

// queries
var (
// go:embed queries/family_create.sql
// familyCreateQuery string
)

// User defines the profile of a user
type User struct {
	CreatedAt *time.Time `json:"created_at,omitempty"`
	Email     string     `json:"email,omitempty"`
	Image     string     `json:"image,omitempty"`
	Name      string     `json:"name,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	UUID      *uuid.UUID `json:"uuid,omitempty"`

	Teams map[uuid.UUID]*Team `json:"teams,omitempty"`
}

// CanEnrich checks if a user can use enrichment credits for a given team
func (u *User) CanEnrich(ctx context.Context, db database.DB, teamUUID uuid.UUID) bool {
	ctx, log := logger.New(ctx, "fivepm.User.CanEnrich", logger.WithField("teamUUID", u.UUID))

	// get team if it hasnt been looked up yet
	// @TODO: check here to make sure the other details exist also
	team, ok := u.Teams[teamUUID]
	if !ok {
		var err error
		team, err = u.GetTeam(ctx, db, teamUUID)
		if err != nil {
			log.WithError(err).Error(ctx, "error getting team")
			return false
		}
	}

	// check if the user is part of the team
	if team.Members == nil {
		log.Error(ctx, "team has no members")
		return false
	}

	member, ok := team.Members[*u.UUID]
	if !ok {
		log.Error(ctx, "user is not a member of the team")
		return false
	}

	if ce, ok := canEnrich[member.Role]; !ok || !ce {
		log.WithField("role", member.Role).Error(ctx, "insufficient permissions to enrich")
		return false
	}

	// get the team credits and make sure there are enough
	if team.Credits == nil || team.Credits.Available <= 0 || team.Credits.Available-team.Credits.Processing <= 0 {
		log.Error(ctx, "team has no credits")
		return false
	}

	return true
}

// GetDetails for a profile
func (u *User) GetDetails(ctx context.Context, db database.DB) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.GetDetails", logger.WithField("uuid", u.UUID))

	// @TODO: move this to a query file
	err := db.Conn.QueryRow(
		ctx,
		`SELECT u."created_at", u."email", u."image", u."name", u."updated_at" FROM "user" u WHERE u."uuid" = $1;`,
		u.UUID,
	).Scan(&u.CreatedAt, &u.Email, &u.Image, &u.Name, &u.UpdatedAt)
	if err != nil {
		log.WithError(err).Error(ctx, "error getting profile details")
		return nil, err
	}

	return u, nil
}

// GetID for a user
func (u *User) GetID(ctx context.Context, db database.DB) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.GetID", logger.WithField("email", u.Email))

	err := db.Conn.QueryRow(
		ctx,
		`SELECT "uuid" FROM "user" WHERE "email" = $1;`,
		u.Email,
	).Scan(&u.UUID)
	if err != nil {
		log.WithError(err).Error(ctx, "error getting user id")
		return u, err
	}

	return u, nil
}

// GetTeam loads all the details for a specific team
func (u *User) GetTeam(ctx context.Context, db database.DB, teamUUID uuid.UUID) (*Team, error) {
	ctx, log := logger.New(ctx, "fivepm.User.GetTeams", logger.WithField("uuid", u.UUID))

	var team Team
	var credits Credits
	var membership Membership

	// get the teams for the user
	err := db.Conn.QueryRow(
		ctx,
		`SELECT 
			t."uuid", 
			t."domain", 
			t."name",
			m."role",
			c."uuid" as credit_uuid,
			c."available" as credits_available,
			c."processing" as credits_processing
		FROM "team" t 
		INNER JOIN "membership" m ON t."uuid" = m."team_uuid"
		INNER JOIN "credits" c ON t."uuid" = c."team_uuid"
		WHERE 
			m."user_uuid" = $1
			AND t."uuid" = $2;
		;`,
		u.UUID,
		teamUUID,
	).Scan(
		&team.UUID,
		&team.Domain,
		&team.Name,
		&membership.Role,
		&credits.UUID,
		&credits.Available,
		&credits.Processing,
	)
	if err != nil {
		// @TODO: improve error handling here to know if the error is db or because the user doesnt belong to the team ID provided
		log.WithError(err).Error(ctx, "error getting the team for the user")
		return nil, err
	}

	team.Credits = &credits

	// add the membership to the team
	membership.UserUUID = u.UUID
	if team.Members == nil {
		team.Members = make(map[uuid.UUID]*Membership)
	}
	team.Members[*u.UUID] = &membership

	// add the team to the users teams
	if u.Teams == nil {
		u.Teams = make(map[uuid.UUID]*Team)
	}
	u.Teams[teamUUID] = &team

	return &team, nil
}

// GetTeams loads all the teams for a user
func (u *User) GetTeams(ctx context.Context, db database.DB) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.GetTeams", logger.WithField("uuid", u.UUID))

	query :=
		`SELECT 
			t."uuid", 
			t."domain", 
			t."name",
			m."role",
			c."uuid" as credit_uuid,
			c."available" as credits_available,
			c."processing" as credits_processing
		FROM "team" t 
		INNER JOIN "membership" m ON t."uuid" = m."team_uuid"
		INNER JOIN "credits" c ON t."uuid" = c."team_uuid"
		WHERE m."user_uuid" = $1;`

	// get the teams for the user
	rows, err := db.Conn.Query(ctx, query, u.UUID)
	if err != nil {
		log.WithError(err).WithField("query", query).Error(ctx, "error getting users teams")
		return nil, err
	}
	defer rows.Close()

	// loop over the rows and add them to the teams map
	if u.Teams == nil {
		u.Teams = make(map[uuid.UUID]*Team)
	}
	for rows.Next() {
		var team Team
		var credits Credits
		var membership Membership

		if err := rows.Scan(
			&team.UUID,
			&team.Domain,
			&team.Name,
			&membership.Role,
			&credits.UUID,
			&credits.Available,
			&credits.Processing,
		); err != nil {
			log.WithError(err).Error(ctx, "error scanning team")
			return nil, err
		}

		team.Credits = &credits

		// add the membership to the team
		membership.UserUUID = u.UUID
		if team.Members == nil {
			team.Members = make(map[uuid.UUID]*Membership)
		}
		team.Members[*u.UUID] = &membership

		u.Teams[*team.UUID] = &team
	}

	return u, nil
}

// Save a profile
// @TODO: save should be reworked in a way that can be used for both new and existing profiles with and without a family
// - maybe the issue is actually in registration and not here ie: save the profile then create the family/membership
func (u *User) Save(ctx context.Context, db database.DB, isNew bool) (*User, error) {
	ctx, log := logger.New(ctx, "fivepm.User.Save", logger.WithField("email", u.Email))

	// create an ID if one doesn't exist
	if u.UUID == nil || *u.UUID == uuid.Nil {
		id := uuid.New()
		u.UUID = &id
	}

	// save the user
	err := db.Conn.QueryRow(
		ctx,
		`INSERT INTO "user" ("uuid", "email", "name", "image") VALUES ($1, $2, $3, $4) 
			ON CONFLICT ("uuid") DO UPDATE SET "email" = $2, "name" = $3, "image" = $4 RETURNING "uuid";`,
		u.UUID,
		u.Email,
		u.Name,
		u.Image,
	).Scan(&u.UUID)
	if err != nil {
		log.WithError(err).Error(ctx, "error saving user")
		return u, err
	}

	return u, nil
}

// Delete removes a profile
func (u *User) Delete(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "fivepm.User.Delete", logger.WithField("email", u.Email))

	log.Info(ctx, "deleting profile")

	// delete the user
	if _, err := db.Conn.Exec(ctx, `DELETE FROM "user" WHERE "uuid" = $1;`, u.UUID); err != nil {
		log.WithError(err).Error(ctx, "failed to delete user")
		return err
	}

	return nil
}
