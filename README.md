# fivepm

fivepm is a package that contains all the core elements of 5pm.

ie: users/teams/credits/etc

## Hierarchy

The overall structure of the `fivepm` package would be

- team
 - memberships
  - user
 - credits

*Notes:*
*- 1 team can have multiple users, 1 user can be part of multiple teams*
*- credits are linked to a single team*